package id.co.telkomsigma.batch.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import id.co.telkomsigma.batch.model.Peserta;

@Component
public class PesertaItemProcessor implements ItemProcessor<Peserta, Peserta>{

	@Override
	public Peserta process(Peserta peserta) throws Exception {
		// TODO Auto-generated method stub
		final Peserta p = new Peserta();
		p.setNama(peserta.getNama().toUpperCase());
		p.setAlamat(peserta.getAlamat());
		p.setTanggalLahir(peserta.getTanggalLahir());
		return p;
	}

}
