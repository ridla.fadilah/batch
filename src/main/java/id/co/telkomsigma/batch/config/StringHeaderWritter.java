package id.co.telkomsigma.batch.config;

import java.io.IOException;
import java.io.Writer;

import org.springframework.batch.item.file.FlatFileHeaderCallback;

public class StringHeaderWritter implements FlatFileHeaderCallback {
	
	private final String header;

	public StringHeaderWritter(String header) {
		this.header = header;
	}

	@Override
	public void writeHeader(Writer writer) throws IOException {
		writer.write(header);
	}

}
