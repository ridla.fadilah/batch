package id.co.telkomsigma.batch.config;

import java.sql.SQLDataException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;

import id.co.telkomsigma.batch.listener.CustomSkipListener;
import id.co.telkomsigma.batch.listener.ItemReaderListener;
import id.co.telkomsigma.batch.mapper.PesertaMapper;
import id.co.telkomsigma.batch.model.Peserta;
import id.co.telkomsigma.batch.processor.PesertaItemProcessor;
import id.co.telkomsigma.batch.writter.PesertaItemWritter;

@Configuration
public class PesertaJobConfiguration {
	
    @Autowired
    public JobBuilderFactory jobBuilderFactory;
    
    @Autowired
    public StepBuilderFactory stepBuilderFactory;
    
    @Autowired
    public PesertaItemWritter itemWritter;
    
    @Autowired
    public PesertaItemProcessor processor;
    
    @Autowired
    public Tasklet deletePesertaTaskLet;

    @Autowired
    public Tasklet sampleTasklet1;
    
    @Autowired
    public Tasklet sampleTasklet2;
    
    @Autowired
    public StepExecutionListener skipCheckingListener;
    
    @Autowired
    public ItemReaderListener readerListener;
    
    @Autowired
    public CustomSkipListener customSkipListener;
    
    @Autowired
    public JobLauncher launcher;
    
    private static Logger logger = LoggerFactory.getLogger(PesertaJobConfiguration.class);
	
	@Bean
    public FlatFileItemReader<Peserta> reader(){
        FlatFileItemReader<Peserta> reader = new FlatFileItemReader<Peserta>();
        reader.setResource(new ClassPathResource("data-peserta.csv"));
        reader.setLineMapper( new DefaultLineMapper<Peserta>() {
        		{
        			setLineTokenizer(new DelimitedLineTokenizer() {
        				{
        					setNames(new String[] {"nama","alamat","tanggalLahir"});
        				}
        			});
        			setFieldSetMapper(new PesertaMapper());
        		}
        	});
			
		return reader;
	}
	
//	@Scheduled(cron = "*/10 * * * * *")
	public void performJob() {
		try {
			logger.info("============= JOB RUN ON {} ================", new Date());
			JobParameters param = new JobParametersBuilder()
					.addString("JobId", String.valueOf(System.currentTimeMillis())).toJobParameters();
			JobExecution execution = launcher.run(importDataPesertaFromCSVJob(), param);
		} catch (Exception ex) {
			logger.error("CANNOT LAUNCH JOB SCHEDULER : {}", ex.getMessage(), ex);
		}
	}
	
	@Bean
	public Job importDataPesertaFromCSVJob() {
        /**
         * simple paralel flow
         **/
		
		Flow splitFlow = new FlowBuilder<Flow>("subFlow")
                .from(step3()).build();
        
        Flow splitFlow2 = new FlowBuilder<Flow>("subFlow2")
                .from(step4()).build();
		
        
        /**
         * paralel flow
         **/
        /*
		Flow splitFlow = new FlowBuilder<Flow>("subFlow")
                .from(step3()).build();
        
        Flow splitFlow2 = new FlowBuilder<Flow>("subFlow2")
                .from(step4()).build();
                
        Flow paralelFlow1 = new FlowBuilder<Flow>("paralelFlow1")
                .start(step1())
                .split(new SimpleAsyncTaskExecutor())
                .add(splitFlow2)
                .build();
        
        Flow paralelFlow2 = new FlowBuilder<Flow>("paralelFlow2")
                .start(paralelFlow1)
                .next(splitFlow)
                .next(splitFlow2)
                .build();
        */
        
        return jobBuilderFactory.get("importDataPesertaFromCsvJob")
                .incrementer(new RunIdIncrementer())
                
		/**
		 * sequential step
		 **/
		/*
                .flow(step1())
                    .next(step2())
                .end()
                .build();
		*/
                
		/**
		 * conditional step
		 **/
		/*
                 .flow(step1())
                    .on("COMPLETED WITH ERROR").to(step3()).next(step2())
                    .from(step1()).on("*").end()
                .end()
                .build();
		*/
		                
		/**
		 * paralel flow
		 **/
        /*
                .flow(step1())
                .split(new SimpleAsyncTaskExecutor()).add(paralelFlow2)
                .end()
                .build();
        */
                
        /**
         * simple paralel flow
         **/
                
                .flow(step1())
                .split(new SimpleAsyncTaskExecutor()).add(splitFlow,splitFlow2)
                .end()
                .build();
        
	}
	
	@Bean
	public Step step1(){  
		return stepBuilderFactory.get("step1")
				.<Peserta, Peserta> chunk(1)
				.reader(reader())
				.processor(processor)
				.writer(itemWritter)
				.faultTolerant()
				.retry(SQLDataException.class)
				.retryLimit(3)
				.skip(FlatFileParseException.class)
				.skip(SQLDataException.class)
				.skipLimit(1)
				.listener(skipCheckingListener)
				.listener(readerListener)
				.listener(customSkipListener)
				.build(); 
	}

	@Bean
	public Step step2(){  
		return stepBuilderFactory.get("step2")
				.tasklet(deletePesertaTaskLet)
				.build();
	}
    
    @Bean
    public Step step3(){
        return stepBuilderFactory.get("step3")
                .tasklet(sampleTasklet1)
                .build();
    }
    
    @Bean
    public Step step4(){
        return stepBuilderFactory.get("step4")
                .tasklet(sampleTasklet2)
                .build();
    }

}
