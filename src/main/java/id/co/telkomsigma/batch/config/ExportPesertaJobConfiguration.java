package id.co.telkomsigma.batch.config;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.FieldExtractor;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import id.co.telkomsigma.batch.model.Peserta;

@Configuration
public class ExportPesertaJobConfiguration {
	
	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	private DataSource dataSource;
	
	@Bean
	public ItemReader<Peserta> databaseCSVItemReader() {
		JdbcPagingItemReader<Peserta> dataReader = new JdbcPagingItemReader<Peserta>();
		dataReader.setDataSource(dataSource);
		dataReader.setPageSize(6);
		dataReader.setRowMapper(new BeanPropertyRowMapper<>(Peserta.class));
		dataReader.setQueryProvider(createQueryProvider());
		
		return dataReader;
	}
	
	public PagingQueryProvider createQueryProvider() {
		MySqlPagingQueryProvider queryProvider = new MySqlPagingQueryProvider();
		queryProvider.setSelectClause("SELECT * ");
		queryProvider.setFromClause("FROM peserta");
		queryProvider.setSortKeys(sortByNamaAsc());
		
		return queryProvider;
	}
	
	private Map<String, Order> sortByNamaAsc() {
		Map<String, Order> sortConfiguration = new HashMap<>();
		sortConfiguration.put("nama", Order.ASCENDING);
		return sortConfiguration;
	}
	
	@Bean
	public ItemWriter<Peserta> databaseCSVWriter() {
		FlatFileItemWriter<Peserta> csvFileWritter = new FlatFileItemWriter<>();
		String exportFileHeader = "id;nama;alamat;tanggalLahir";
		csvFileWritter.setHeaderCallback(new StringHeaderWritter(exportFileHeader));
		csvFileWritter.setResource(new FileSystemResource("export-data Peserta.csv"));
		
		csvFileWritter.setLineAggregator(createPesertaLineAggregator());
		return csvFileWritter;
	}
	
	private LineAggregator<Peserta> createPesertaLineAggregator() {
		DelimitedLineAggregator<Peserta> delimitedLineAggregator = new DelimitedLineAggregator<Peserta>();
		delimitedLineAggregator.setDelimiter(";");
		delimitedLineAggregator.setFieldExtractor(createPesertaFieldExtractor());
		return delimitedLineAggregator;
	}
	
	private FieldExtractor<Peserta> createPesertaFieldExtractor() {
		BeanWrapperFieldExtractor<Peserta> extractor = new BeanWrapperFieldExtractor<>();
		extractor.setNames(new String[] {"id", "nama", "alamat", "tanggalLahir"});
		return extractor;
	}
	
	@Bean
	public Step databaseToCSVStep(ItemReader<Peserta> itemReader, ItemWriter<Peserta> itemWriter) {
		return stepBuilderFactory.get("databaseToCSVStep")
				.<Peserta, Peserta> chunk(1)
				.reader(itemReader)
				.writer(itemWriter)
				.build();
	}
	
	@Bean
	public Job exportPesertaJob() {
		return jobBuilderFactory.get("exportPesertaJob")
				.incrementer(new RunIdIncrementer())
				.flow(databaseToCSVStep(databaseCSVItemReader(), databaseCSVWriter()))
				.end()
				.build();
	}

}
