package id.co.telkomsigma.batch.writter;

import java.sql.SQLDataException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import id.co.telkomsigma.batch.dao.PesertaDao;
import id.co.telkomsigma.batch.model.Peserta;
import lombok.Getter;
import lombok.Setter;

@Component
@StepScope
public class PesertaItemWritter implements ItemWriter<Peserta>{
	
	Logger logger = LoggerFactory.getLogger(PesertaItemWritter.class);
		
	@Autowired
	PesertaDao pesertaDao;
	
	@Getter @Setter @Value("#{jobParameters['JobId']}")
	private String JobId;
	
	@Override
	public void write(List<? extends Peserta> arg0) throws Exception {
		logger.info("SEND PARAMETER JOB ID : "+JobId);
        for (Peserta peserta : arg0) {
            if(peserta.getNama().equalsIgnoreCase("Vincent")){
                throw new SQLDataException("ENABLE TRY ERROR !!");
            }
            
            logger.info("PESERTA SAVING : {}",peserta.getNama());
            pesertaDao.save(peserta);
              
        }
	}

}
