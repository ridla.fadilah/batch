package id.co.telkomsigma.batch.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.annotation.AfterRead;
import org.springframework.batch.core.annotation.BeforeRead;
import org.springframework.batch.core.annotation.OnReadError;
import org.springframework.stereotype.Component;

import id.co.telkomsigma.batch.model.Peserta;

@Component
public class ItemReaderListener {
	Logger logger = LoggerFactory.getLogger(ItemReaderListener.class);

    @BeforeRead
    public void beforeRead() {
        logger.info("INTERCEPTOR SEBELUM BACA FILE");
    }

    @AfterRead
    public void afterRead(Peserta p) {
        logger.info("INTERCEPTOR SETELEAH BACA FILE : {}",p);
    }
    
    @OnReadError
    public void onReadError(Exception ex) {
        logger.error("INTERCEPTOR KETIKA ADA YANG ERROR : {}",ex.getMessage());
    }

}
