package id.co.telkomsigma.batch.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.co.telkomsigma.batch.model.Peserta;

public interface PesertaDao extends PagingAndSortingRepository<Peserta, String> {
	
	
}
