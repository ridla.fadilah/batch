package id.co.telkomsigma.batch.controller;

import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PesertaBatchJobController {
	
	@Autowired
	private JobLauncher jobLauncher;
	
	@Autowired
	private Job importDataPesertaFromCSVJob;
	
	@Autowired
	private Job exportPesertaJob;
	
	private Logger logger = Logger.getLogger(PesertaBatchJobController.class);
	
	@GetMapping("/runPesertaJob/{JobId}")
	public String runPesertaJob(@PathVariable("JobId") String JobId) {
		try {
			JobParameters jobParameters = new JobParametersBuilder().addString("JobId", JobId).toJobParameters();
			jobLauncher.run(importDataPesertaFromCSVJob, jobParameters);
		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException :", e);
		} catch (JobRestartException e) {
			logger.error("JobRestartException :", e);
			e.printStackTrace();
		} catch (JobInstanceAlreadyCompleteException e) {
			logger.error("JobInstanceAlreadyCompleteException :", e);
		} catch (JobParametersInvalidException e) {
			logger.error("JobParametersInvalidException :", e);
		}
		return "Done";
	}
	
	@GetMapping("/runExportPesertaJob/{JobId}")
	public String runExportPesertaJob(@PathVariable("JobId") String JobId) {
		try {
			JobParameters jobParameters = new JobParametersBuilder().addString("JobId", JobId).toJobParameters();
			jobLauncher.run(exportPesertaJob, jobParameters);
		} catch (JobExecutionAlreadyRunningException e) {
			logger.error("JobExecutionAlreadyRunningException :", e);
		} catch (JobRestartException e) {
			logger.error("JobRestartException :", e);
			e.printStackTrace();
		} catch (JobInstanceAlreadyCompleteException e) {
			logger.error("JobInstanceAlreadyCompleteException :", e);
		} catch (JobParametersInvalidException e) {
			logger.error("JobParametersInvalidException :", e);
		}
		return "Done";
	}

}
