package id.co.telkomsigma.batch.tasklet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

public class SampleTasklet2 implements Tasklet {

	Logger logger = LoggerFactory.getLogger(SampleTasklet2.class);

    @Override
    public RepeatStatus execute(StepContribution sc, ChunkContext cc) throws Exception {
        logger.info("RUN SAMPLE TASKLET 2 !!");
        return null;
    }
    
}
