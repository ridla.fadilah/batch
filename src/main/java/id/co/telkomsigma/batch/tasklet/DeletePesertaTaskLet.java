package id.co.telkomsigma.batch.tasklet;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
public class DeletePesertaTaskLet implements Tasklet {
	
	private static Logger logger = LoggerFactory.getLogger(DeletePesertaTaskLet.class);

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		File file = new ClassPathResource("data-peserta.csv").getFile();
		if(file.delete()) {
			logger.info("File {} Deleted.", file.getName());
		}
		return RepeatStatus.FINISHED;
	}

}
