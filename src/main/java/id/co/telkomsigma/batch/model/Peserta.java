package id.co.telkomsigma.batch.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Table
@Entity
public @Data class Peserta implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -892804912402969059L;
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	private String id;
	private String nama;
	private String alamat;
	@Temporal(TemporalType.DATE)
	private Date tanggalLahir;

}
