package id.co.telkomsigma.batch.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import id.co.telkomsigma.batch.model.Peserta;

public class PesertaMapper implements FieldSetMapper<Peserta>{

	@Override
	public Peserta mapFieldSet(FieldSet arg0) throws BindException {
		// TODO Auto-generated method stub
		
		Peserta peserta = new Peserta();
		
		peserta.setNama(arg0.readRawString(0));
		peserta.setAlamat(arg0.readRawString("alamat"));
		peserta.setTanggalLahir(arg0.readDate(2));
		
		return peserta;
	}

}
